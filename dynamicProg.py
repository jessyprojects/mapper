#!/usr/bin/python3
# -*- coding: utf-8 -*-

# compatible python3

class DynamicMatrix:
	'''stores a matrix |S|x|T| (|S|+1 lines and |T|+1columns), sequences S and T and the score system (match, mismatch, gap)
	defines some global alignment functions
	'''
	def __init__(self, S: str, T: str, match: int, mismatch: int, gap: int, dMax: int):
		''' defines and stores initial values'''
		self.S = S
		self.T = T
		self.gap = gap
		self.match = match
		self.mismatch = mismatch
		self.dMax = dMax
		self.matrix = [0 for i in range(len(S)+1)]
		for i in range(len(S)+1):
			self.matrix[i] = [0 for j in range(len(T)+1)]
        	# self.matrix[i][j]
        	# i : ième line, in S
        	# j : jième column, in T
        	 
	def printMatrix(self):
		''' prints the matrix'''
		width = 4
		vide = " "
		line = f"{vide:>{2*width}}"
		for j in range(0,len(self.T)):
			line += f"{self.T[j]:>{width}}"
		print(line)
		line = f"{vide:>{width}}"
		for j in range(0,len(self.T)+1):
			line += f"{self.matrix[0][j]:>{width}}"
		print(line)
		for i in range(1,len(self.S)+1):
			line = f"{self.S[i-1]:>{width}}"
			for j in range(0,len(self.T)+1):
				line += f"{self.matrix[i][j]:>{width}}"
			print(line)
            


	# attribution du score entre 2 caractères
	def score(self, A: str, B: str) -> int:
		if A == B:   # Caractères égaux => match
			return self.match
		else:           # Caractères Différents => match
			return self.mismatch

	# remplissage de la matrice selon les régles de match, mismatch et gap
	def fill(self):
		''' fills the matrix for global alignment (Needleman & Wunsch algo)'''
		for i in range(1,len(self.S)+1):
			# i-th line
			for j in range(1,len(self.T)+1):
				# j-th column
				self.matrix[i][j]=max(self.matrix[i-1][j-1]+self.score(self.S[i-1],self.T[j-1]),
						self.matrix[i][j-1]+self.gap,
						self.matrix[i-1][j]+self.gap)

	# initialisation Semi Globale
	def initSemiGlobal(self):
		for i in range(1,len(self.matrix)):
			self.matrix[i][0]=self.matrix[i-1][0]+self.gap

	def getSemiGlobalAln(self):
		''' Gets the best score over the whole last columns and returns the alignments of S against a sub-string of T with best score'''
        	# self.score(self.S[i-1],self.T[j-1])
		# getting max score over last line, and teh corresponding column indice jmax
		jmax = 1
		slen = len(self.S)
		scoreMax = self.matrix[slen][jmax]
		for j in range(2,len(self.T)+1):
			if self.matrix[slen][j]>scoreMax:
				jmax = j
				scoreMax = self.matrix[slen][j]
		# backtracking the alignment
		if abs(scoreMax) > self.dMax:
			return -1
		i=slen
		j=jmax
		while i>0:
			if j > 0 and (self.matrix[i-1][j-1]+self.score(self.S[i-1],self.T[j-1]) == self.matrix[i][j]):
				# diag
				i -= 1
				j -= 1
			elif j > 0 and (self.matrix[i][j-1]+self.gap == self.matrix[i][j]):
				# left
				j -= 1
			else:
				# up
				i -= 1
		return j, abs(scoreMax)
# TESTS
#S = read, T = region
'''
print("******** Semi Global **********")

dm = DynamicMatrix("TCTGATGTAGCAATGCGGTTTCCCTTGAAGGATAGGCATAATTTGATTGATCACTTCACTGCGATCTAGGTATGTTTAGAGTAATAGTTTCCAACCCACT","ACGTCTGATGTAATGCGGTTTCCCTTGAAGGATAGGCATAATTTGATTGATCACTTCACTGCGATCTAGCTATGTTTAGAGTAATAGTTTCCAACCCACT", 0, -1, -1, 10)

dm = DynamicMatrix("ACGTCTGATGTAATGCGGTTTCCCTTGAAGGATAGGCATAATTTGATTGATCACTTCACTGCGATCTAGCTATGTTTAGAGTAATAGTTTCCAACCCACT", "TCTGATGTAGCAATGCGGTTTCCCTTGAAGGATAGGCATAATTTGATTGATCACTTCACTGCGATCTAGGTATGTTTAGAGTAATAGTTTCCAACCCACT", 0, -1, -1, 10)
dm.initSemiGlobal()
dm.fill()
dm.printMatrix()
pos,nberrors = dm.printSemiGlobalAln()
print(f"nb errors : {nberrors} at position {pos} in T")
'''

