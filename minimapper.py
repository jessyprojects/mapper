#!/usr/bin/python3
from guppy import hpy
from BWT import *
from dynamicProg import *
from Timer import *

class MiniMapper:
	'''
	Constructor class of minimapper
	
	Parameters :
		- referenceName : path name to the reference file
		- readsName : path name to the reads file
		- fileOutName : name of the result file
		- match : value for a match (0 by default)
		- mismatch : value for a mismatch (-1 by default)
		- gap : value for a gap (-1 by default)
		- dMax : maximum number of errors allowed
		- k : size of the seed 
	'''
	def __init__(self, referenceName: str, readsName: str, fileOutName: str, match: int, mismatch: int, gap: int, dMax: int, k: int):
		''' defines and stores initial values'''
		self.referenceName = referenceName
		nameDump = "Pickle/" + referenceName[referenceName.rfind("/") + 1:].replace('.fasta','.dump.gz')
		self.loadBWT(nameDump)
		self.reads = open(readsName, 'r')
		self.fileOut = open(fileOutName, 'w')
		self.gap = gap
		self.match = match
		self.mismatch = mismatch
		self.dMax = dMax
		self.k = k
	
	'''
	Loading or creating the Burrows-Wheeler depending on whether it already exists or not
	'''
	def loadBWT(self, nameDump: str):
		with Timer() as bwt_time:
			if(os.path.exists(nameDump)):
				self.bwt = pickle.load(gzip.open(nameDump,"rb"))
				self.sequence = self.bwt.S
			else:
				self.reference = open(self.referenceName, 'r')
				self.reference.readline()
				self.sequence = self.reference.readline()
				self.reference.close()
				self.bwt = BWT(self.sequence)
				pickle.dump(self.bwt , gzip.open(nameDump,"wb"))
		bwt_time.print('Duration BWT Generation = {} seconds')
	
	'''
	Returns a sub-sequence of the reference genome from a given position obtained from a perfect alignment of a sub-sequence of the read (ssr) and on which we will try to align the read
	
	Parameters :
		sequence : the genome reference
		posInRef : position of ssr in the genome reference
		posInRead : position of ssr in the read
		readSize : size of the read
	'''
	def getRegion(self, sequence: str, posInRef: int, posInRead: int, readSize: int) -> str:
		# to have all the possible alignments of the read on the region with less than dMax errors,
		# we limit the size of the region by the size of the read with +/- dmax errors on each side
		start = posInRef - (posInRead + self.dMax)
		end = start + readSize + 2 * self.dMax
		if start >= 0 & end < len(sequence) :
			return sequence[start:end]
		else:
			# if the starting position is negative,  we return an alignment only if the difference is less than dMax errors
			# otherwise, there will be no alignment with less than dMax errors
			if start < 0:
				if -start - self.dMax > self.dMax:
					return ""
				else:
					return sequence[:end-start]
			# same logic in the case the end position exceeds the size of the sequence
			else:
				if end - (len(sequence) + self.dMax) > self.dMax:
					return ""
				else:
					return sequence[start:]
	
	'''
	Returns the reverse complement of the sequence given in parameter
	'''
	def getReverse(self, read: str) -> str:
		result = ""
		for i in range(len(read)):
			if read[i] == 'A':
				result += 'T'
			elif read[i] == 'T':
				result += 'A'
			elif read[i] == 'G':
				result += 'C'
			elif read[i] == 'C':
				result += 'G'
		return result[::-1]
	
	'''
	Returns : 
		- returns the best alignment of the read on the sequence with the associated number of errors
		- or -1 if no alignment was found

	Parameters :
		- sequence : the genome reference
		- readName : name of the read
		- read : content of read
		- sign : sign distinguishing the read (+) from its reverse complement (-)
	'''
	def getBestAlign(self, sequence: str, readName: str, read: str, sign: str) -> (int, str):
		dMin = self.dMax + 1
		# best alignment result
		result = ""
		# list containing all the positions of the genome already tested so as not to align the same region several times on the same read
		listPos = []
		for i in range(len(read) - self.k + 1):
			# get the positions of the seed in the reference genome 
			# see BWT class
			pos = self.bwt.whereIsPinS(read[i:i+self.k])
			if pos != -1:
				for p in pos:
					# checks if that region has not already been tested for the current read
					if (p-(i+self.dMax)) not in listPos:
						listPos.append(p-(i+self.dMax))
						# we get the region from the position
						region = self.getRegion(sequence, p, i, len(read))
						if region != "":
							# we get the alignment of the read on the region
							# see dynamicProg class
							dm = DynamicMatrix(read, region, self.match, self.mismatch, self.gap, self.dMax)
							dm.initSemiGlobal()
							dm.fill()
							dmResult = dm.getSemiGlobalAln()
							if dmResult != -1:
								# if we get a better alignment, we update the results
								if dmResult[1] < dMin:
									dMin = dmResult[1]
									# meilleur alignement actuel
									finalPos = 0 if p - (i +self.dMax) < 0 else p - (i + self.dMax)
									result = readName[1:-1] + "	" + str(finalPos + dmResult[0]) + "	" + sign + "	" + str(dMin) + "\n"
									if dMin == 0: return dMin, result
		
		if result == "" :
			return -1
		else: 
			return dMin, result
	
	'''
	Read each read one by one and add in the output file the best alignment between the read and its reverse complement
	'''
	def readAllReads(self):
		h = hpy()
		readName = self.reads.readline()
		read = ""
		# number of alignment found
		countWrite = 0
		# result of the best alignment of the read
		resultRead = -1
		# result of the best alignment of its reverse complement
		resultRevRead = -1
		# init fileOut
		self.fileOut.write("#id	pos	brin	d\n")
		print("In progress...")
		with Timer() as total_time:
			while readName != "":
				read = (self.reads.readline())[:-1]
				resultRead = self.getBestAlign(self.sequence, readName, read, "+")
				# if we get a perfect alignment for the read, we do not calculate the alignment for its reverse complement
				# because we already know that there will be no better solution
				if resultRead != -1 and resultRead[0] == 0:
					self.fileOut.write(resultRead[1])
					countWrite += 1
				else:
					resultRevRead = self.getBestAlign(self.sequence, readName, self.getReverse(read), "-")
					if resultRead != -1 or resultRevRead != -1:
						countWrite += 1
						if resultRevRead == -1:
							self.fileOut.write(resultRead[1])
							
						elif resultRead == -1:
							self.fileOut.write(resultRevRead[1])
							
						else:
							if resultRevRead[0] < resultRead[0]:
								self.fileOut.write(resultRevRead[1])
							else:
								self.fileOut.write(resultRead[1])
				
				readName = self.reads.readline()
		print("Done")
		total_time.print('Duration = {} seconds')
		print("Number Align : ",countWrite)
		#print memory used
		#print(h.heap())
		#print(x.size)
		self.reads.close()
		self.fileOut.close()

'''
use of a parser to manage program options

see user manual for more details
'''

import argparse

#python3 minimapper.py -ref test1/reference.fasta -reads test1/reads.fasta -k 15 -dmax 4 -out result-k15-dmax4.txt
#python3 minimapper.py -ref test2/genome_ecoli_100kb.fasta -reads test2/reads.fasta -k 20 -dmax 4 -out result-k20-dmax4.txt

parser = argparse.ArgumentParser()
parser.add_argument("-ref", help ="File input that contains Reference", type=str, required=True)
parser.add_argument("-reads", help ="File input that contains all the Reads", type=str, required=True)
parser.add_argument("-k", help ="Size of Seeds", type=int, required=True)
parser.add_argument("-dmax", help ="Maximum number of allowed differences", type=int, required=True)
parser.add_argument("-out", help ="File output ", type=str, required=True)


args = parser.parse_args()
'''
ref = args.ref
reads = args.reads
k = args.k
dmax = args.dmax
out = args.out
'''
match = 0
mismatch = -1
gap = mismatch

mp = MiniMapper(args.ref, args.reads, args.out, match, mismatch, gap, args.dmax, args.k)
mp.readAllReads()
