#!/usr/bin/python3
# -*- coding: utf-8 -*-
import pickle
import gzip
import os.path

class BWT:
	'''
	Constructor class of BWT
	
	Builds the Burrows–Wheeler transform of the sequence given in parameter
	'''
	def __init__(self, sequence: str):
		self.S = sequence
		self.SA = self.get_SA()
		self.bwt = self.build(self.SA)
		self.N, self.R = self.get_N_R()

	'''
	Returns the suffix array of the sequence
	'''
	def get_suff(self, n: int) -> list:
		tab = []
		for i in range(n):
			tab.append(self.S[i:n])
		return tab
	
	'''
	Returns the index table of sorted suffixes
	'''
	def get_SA(self) -> list:
		n = len(self.S)
		tabS = sorted(self.get_suff(n))
		SA = []
		for i in range(n):
			SA.append(n - len(tabS[i]))
		return SA
		
	'''
	Returns the Burrows Wheeler transform from the suffix array
	'''
	def build(self, SA: list) -> str:
		return ''.join([self.S[i-1] if i else '$' for i in SA])
	
	'''
	Returns :
		- N : the number of occurrences of each character in 
		- rank : the rank of each character in bwt
	'''
	def get_N_R(self) -> (dict, list):
		R = []
		N = {'A':0, 'T':0, 'C':0, 'G':0, '$':0}
		for letter in self.bwt:
			N[letter] += 1
			R.append(N[letter]-1)
		return N, R
	
	'''
	Returns :
		s + k : the position in N of the character c of rank k with :
			- s : index of the first occurence of c in N
			- k : rank of s in N
	'''
	def lf(self, alpha: str, k: int, N: list):
		a = ['$', 'A', 'C', 'G', 'T']
		if alpha == '$':
			return 0
		s = 0
		for i in range(len(a)):
			if alpha > a[i]:
				s += N[a[i]]
			else:
				return s + k
        
	'''
	Returns :
		i : index of the first character c such that c = bwt[i]
	'''
	def find_first(self, c: int, i: int, BWT: str) -> int:
		while i < len(BWT) and c != BWT[i]:
			i+=1
		return i
	
	'''
	Returns :
	    # j : index of the last caractère c such that c = bwt[j] 
	'''
	def find_last(self, c: int, j: int, BWT: str) -> int:
		while j > -1 and c != BWT[j]:
			j-=1
		return j
	    
	'''
	Returns :
		- the list of occurrences of P in the sequence
		- -1 if no occurrence of P exists in the sequence
	'''
	def whereIsPinS(self, P: str) -> list:
		# lower bound
		i = self.lf(P[-1], 0, self.N)
		# upper bound
		j = i + self.N[P[-1]] - 1
		# we continue the search with the penultimate character of the pattern
		pos_p = len(P) - 2
		while pos_p >= 0:
			# we look for the index of the next character in P from of the lower bound
			first = self.find_first(P[pos_p], i, self.bwt)
			# if the index is bigger than the upper bound then the pattern does not exist in the sequence
			if first > j: return []
			# new lower bound
			i = self.lf(P[pos_p], self.R[first], self.N)
			last = self.find_last(P[pos_p], j, self.bwt)
			# new upper bound
			j = self.lf(P[pos_p], self.R[last], self.N)
			pos_p -= 1
		# i and j allow to know the interval in which our pattern is present
		# positions in the sequence are contained in SA
		return [self.SA[p] for p in range(i, j+1)]



#tests
'''
seq = "CGAGCTGGTCCTAACCCGGAGACCGCAGGCTGCGCGCGTATCGCAGCATCTGGCATTACGCCGCATCGAGTGCATGCACGAGAGAAGGAAGGGCACTGTTCGCTACCAGTCTACCCTACATAAGATTATACACATCTTTGAGTTTTTTCGTCCATCAAGTAGCGAAACGGATGTAGCGCTTCCCGACGGACTTCATAGGCGATTCACTCACGGTCGATTGAGCCGGGCGGAGCATGCTACACGTGTAAATGTTCTCGGTTAACTATTATGGTTTGGATGATTGGTGCCAGTGTTGCTTGCGTCTGACGAGTACACACCCTATAGAGAAAGAATACCTCATGTTTGCGTAACGAGCGTTCAATTTCCTCCTGTTTGTACCTTACCCCGAGGGTTATCGAACCTTGCGGGCTGGGTCGGAAAACTTGTCTTAGAGGCCTGCGACCGTGATTACATTGCTACAGATTGTCCCCATTGTTCCGCGGAGGCATTTTCGCAGGACGTCTGATGTAATGCGGTTTCCCTTGAAGGATAGGCATAATTTGATTGATCACTTCACTGCGATCTAGCTATGTTTAGAGTAATAGTTTCCAACCCACTGAGGCACTCTCGTTCTGTGAAACAGTTAGGCCGGTTGCCGTGCGCAGCAACATGGTGTGGACACATCTCCCAGCCTGTTGAATGACGGCCTAGTGTCAGGAATTAGAGAGCCTTAACCCTATCAGGGTTGTCCCGACAGTTGACATCGCCCGAGATGGCTCTTTTGAAGGGCCCCAAGATCGGCTGCATCTACTTGGCACAACGGCTTTGCCTGGCTCGTTAAAATCCTGTCACATACGCGAGTTCCCGAAGTTGGCCGATTGCCCCTATCACCGTGTTGGAACCCATGTGTTAGCACAGACCTGAAGACTAATCCTCATTCCCTGTGTCACCGCAATTTCAGCCAAGCCAGCCACGCGCTCCTTGTTAGTCGTATATGGCGTTAATGAGCTTCAAACCCCGA$"

seq2 = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA$"
bwt = BWT("bb", seq)
SA = bwt.get_SA()
mybwt = bwt.build(SA)
N,R = bwt.get_N_R(mybwt)
#print(bwt)
w = "ATAGTTTCCAACCCACTGAG"
pos = bwt.whereIsPinS(w, mybwt, N, R, SA)
print(pos)
'''
